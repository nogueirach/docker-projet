1) Télécharger le projet à partir du lien: 
2) Mettre le dossier SuperSite dans votre var/www/html, n'oubliez pas de changer les droits si besoin
3) Taper la commande xhost + dans votre terminal
4) Taper docker-compose  up -d 
5) Trouver l'ip du localhost en tapant:
                            -> "docker ps" et récupérer l'ip du docker apache
                            -> "ip a" pour récupérer l'ip de votre poste
6) Taper dans le firefox ouvert par le docker l'adresse suivante: ipDeVotrePoste:ipDuDockerApache/SuperSite/index.php
